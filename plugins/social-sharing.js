

const socialHelper = {

  getSocialMeta: function(title, description, imageUrl) {
    return [
      {
        hid: 'twitter:title',
        name: 'twitter:title',
        content: title
      },
      {
        hid: 'twitter:description',
        name: 'twitter:description',
        content: description
      },
      {
        hid: 'twitter:image',
        name: 'twitter:image',
        content: imageUrl
      },
      {
        hid: 'twitter:image:alt',
        name: 'twitter:image:alt',
        content: title
      },
      {
        hid: 'og:title',
        property: 'og:title',
        content: title
      },
      {
        hid: 'og:description',
        property: 'og:description',
        content: description
      },
      {
        hid: 'og:image',
        property: 'og:image',
        content: imageUrl
      },
      {
        hid: 'og:image:secure_url',
        property: 'og:image:secure_url',
        content: imageUrl
      },
      {
        hid: 'og:image:alt',
        property: 'og:image:alt',
        content: title
      }
    ]
  },

}

export default (context, inject) => {

  inject('socialHelper', socialHelper)
}






