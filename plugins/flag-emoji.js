export default (context, inject) => {
    const flagEmoji = function(countryCode) {
        // let countryCodeRaw = flag.codePointAt()
        //let countryCode = String.fromCharCode(countryCodeRaw-127397).toLowerCase()
        // var countryCode = Array.from(flag, (codeUnit) => codeUnit.codePointAt()).map(char => String.fromCharCode(char-127397).toLowerCase()).join('')

        return "<img src='https://flagcdn.com/24x18/" + countryCode + ".png?v=2.0'>"
    }
    inject('flagEmoji', flagEmoji)
}
