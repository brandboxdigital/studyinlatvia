

function fStatically(url, width, height)  {

  if (!url) {
    return '';
  }

  if (typeof url === 'object') {

    if (Object.keys(url).indexOf('path') > -1) {
      url = url.path;
    } else {
      return '';
    }
  }

  // Check if we need to run Nuxt in development mode
  const isDev = process.env.NODE_ENV !== 'production'

  if (isDev) {
    return url;
  }

  /**
   * Already is stacally link..
   */
  if (url.indexOf('cdn.statically.io') !== -1) {
    return url;
  }

  let link   = url;
  let domain = 'localhost';

  try {
     domain = $nuxt.$config.TRUE_HOST;
  } catch (error) {}

  if (url.indexOf('http://') === -1 && url.indexOf('https://') === -1) {
  } else {
    const noHttp = url.replace('http://','').replace('https://','');

    domain = noHttp.split(/[/?#]/)[0];
    link   = noHttp.replace(domain+"/", '');

  }

  domain = domain.replace('http://','').replace('https://','').replace(/\/$/, '');

  if (width && height) {
    return `https://cdn.statically.io/img/${domain}/q=90,f=auto,w=${width},h=${height}/${link}`;
  } else if (width) {
    return `https://cdn.statically.io/img/${domain}/q=90,f=auto,w=${width}/${link}`;
  } else {
    return `https://cdn.statically.io/img/${domain}/q=90,f=auto/${link}`;
  }

}


export default (context, inject) => {

  // const statically = function(flag) {

  // }

  inject('statically', fStatically)
}
