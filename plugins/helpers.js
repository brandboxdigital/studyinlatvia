import _get from "lodash/get";

/**
 * $dayjs format stirngs.
 */
export const FShorterDate = 'YYYY-MM-DD'
export const FD = 'DD.MM.YYYY'
export const FT = 'HH:mm'
export const FF = 'DD.MM.YYYY HH:mm'

export function sanitizeUrl(url) {
  if (url.indexOf('http://') === -1 && url.indexOf('https://') === -1) {
    return `https://${url}`;
  }

  return url;
}


export function removeQueryAndHashtags(url) {
  return url.replace(/[\?|\#].*/, '');
}


export function mediaLibraryUrl(path) {
  if (path) {
    let backend_url = process.env.BACKEND_URL || null;
    try {
      backend_url = $nuxt.$config.BACKEND_URL;
    } catch (error) {}
    if (backend_url == null) {
      return path;
    }
    if (path.indexOf('http://') === -1 && path.indexOf('https://') === -1) {
      if (path[0] == '/') {
        return `${backend_url}/storage/app/media${path}`;
      } else {
        return `${backend_url}/storage/app/media/${path}`;
      }
    }
    return path;
  }
}


export function safeGet(obj, param, def = null) {
  return _get(obj, param, def);
}


export function mapTestimonialsAndStories(item, tabSlug) {
  return {
    country: safeGet(item, "country.title"),
    countryFlag: safeGet(item, "country.short_code"),
    name: `${safeGet(item, 'name')} ${safeGet(item, 'surname')}`,
    quote: item.title,
    image: item.image,
    video: item.video,
    to: {
      name: "article-tab-slug",
      params: {
        tab: tabSlug,
        slug: item.slug,
      },
    },
  };
}

export function convertToBoolean(val) {
  if (val && val == '1') {
    return true
  } else {
    return false
  }
}

export function convertBlock(block) {
  let blockConverted = block
  blockConverted.img = mediaLibraryUrl(block.img)
  return blockConverted
}
