export const state = () => ({
  tab: null,
})

export const mutations = {
  SET_TAB: (state, payload) => {
    state.tab = payload
  }
}
