export const state = () => ({
  studies: {},
})

export const mutations = {
  SET_STUDIES: (state, payload) => {
    state.studies = payload
  }
}

export const actions = {
  findStudies ({ commit }) {
    return new Promise((resolve, reject) => {
      this.$axios.get('/api/v1/universities/study-programmes').then((r) => {
        commit('SET_STUDIES', r.data)
        resolve(r.data)
      }).catch((e) => {
        reject(e)
      })
    })
  }
}

