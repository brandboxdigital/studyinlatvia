export const state = () => ({
  admissions: [],

  loaded: false,
  tab: null,
})

export const mutations = {
  setTab: (state, payload) => {
    state.tab = payload;
  },

  setAddmisions: (state, payload) => {
    if (payload && payload.data) {
      state.admissions = payload.data;
      state.loaded = true;
    }
  }
}

export const actions = {
  async getData({ commit }, params) {

    const [
      addmisions,
    ] = await Promise.all([
      this.$axios.$get('/api/v1/content/admissions'),
    ]);

    commit('setAddmisions', addmisions);

    if (params) {
      commit('setTab', params.tab);
    }

  },
}



