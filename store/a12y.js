export const state = () => ({
  contrast: null,
  textSize: 100,
  readingMode: false
})

export const mutations = {
  setContrast: (state, payload) => {
    state.contrast = payload;
  },

  setTextSize: (state, payload) => {
    state.textSize = payload;
  },

  setReadingMode: (state, payload) => {
    state.readingMode = payload;
  },

}
