export const state = () => ({
    loading: true,
})

export const mutations = {
    loading: (state) => {
        state.loading = !state.loading
    },
}

