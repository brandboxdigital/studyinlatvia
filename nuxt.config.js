import axios from 'axios';
import stringInject from 'stringinject';



export default {
  // Target (https://go.nuxtjs.dev/config-target)
  target: 'server',
  // ssr: true,

  publicRuntimeConfig: {
    TRUE_HOST: process.env.TRUE_HOST     || 'https://studyinlatvia.lv/',
    BACKEND_URL: process.env.BACKEND_URL || 'https://study-in-latvia.brandbox.digital',
    gtm: {
      id: process.env.GOOGLE_TAG_MANAGER_ID
    }
  },

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'Study In Latvia',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.png'}
    ],
    script: [
      // {
      //   src: 'https://www.googletagmanager.com/gtag/js?id=G-',
      //   async: true,
      // },
      // {
      //   src: '/ga4.js',
      // },
    ],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    '@/assets/styles/main.scss',
    "vue-slick-carousel/dist/vue-slick-carousel.css"
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    { src: "~/plugins/vue-slick-carousel.js" },
    { src: '~/plugins/axios.js' },
    { src: "~/plugins/aos.js", mode: "client" },
    { src: "~/plugins/flag-emoji.js"},
    { src: '~/plugins/statically' },
    { src: '~/plugins/social-sharing' },
    { src: '~/plugins/vue-masonry', mode: "client" },
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: {
    dirs: [
      '~/components',
      '~/components/base',
      '~/components/partials',
      '~/components/assets',
    ]
  },

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    '@nuxtjs/style-resources',
    // '@nuxtjs/google-analytics',
  ],

  styleResources: {
    scss: [
      '@/assets/styles/_variables.scss',
      '@/assets/styles/_fonts.scss',
      '@/assets/styles/_bulma_utilities.scss',
    ],
  },

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // Remove css because manually included in css
    ['nuxt-buefy', { css: false }],

    'nuxt-ssr-cache',

    '@nuxtjs/gtm',

    '@nuxtjs/axios',
    'nuxt-user-agent',
    'nuxt-clipboard2',
    'nuxt-mq',
    '@nuxtjs/dayjs',

    'vue-social-sharing/nuxt',

    '@nuxtjs/sitemap',
    '@nuxtjs/redirect-module',
  ],

  mq: {
    // Default breakpoint for SSR
    defaultBreakpoint: 'lg',
    breakpoints: {
      sm: 450,
      md: 1050,
      lg: Infinity
    }
  },

  // https://www.npmjs.com/package/@nuxtjs/dayjs
  dayjs: {
    defaultTimeZone: 'UTC',
    plugins: [
      'utc',
      'timezone',
      'isoWeek', // https://day.js.org/docs/en/plugin/iso-week
      'isBetween', // https://day.js.org/docs/en/plugin/is-between
      'advancedFormat', // https://day.js.org/docs/en/plugin/advanced-format
      'customParseFormat', // https://day.js.org/docs/en/plugin/custom-parse-format
    ]
  },
  gtm: {
    id: 'GTM-WQS2HCZ'
  },

  // googleAnalytics: {
  //   id: 'UA-28448965-1',
  //   dev: process.env.NODE_ENV !== 'production',
  // },

  axios: {
    baseURL: process.env.BACKEND_URL || 'https://cms.studyinlatvia.lv',
    //http://study.vagrant.test
  },

  sitemap: {
    hostname: process.env.TRUE_HOST || 'https://studyinlatvia.lv',
    i18n: false,

    defaults: {
      changefreq: 'daily',
      priority: 1,
      lastmod: new Date()
    },

    /* routes: async () => {
      const toRet = [];
      const axiosBaseUrl = process.env.BACKEND_URL || 'https://cms.studyinlatvia.lv';


      const [
        news,
        events,
        studenttestimonials,
        experiencestories,

        universities,
        studies,

        termsCategories,
        faqsCategories,
        admissionsCategories,
      ] = await Promise.all([
        axios.get(`${axiosBaseUrl}/api/v1/news/news`),
        axios.get(`${axiosBaseUrl}/api/v1/news/events`),
        axios.get(`${axiosBaseUrl}/api/v1/news/student-testimonials`),
        axios.get(`${axiosBaseUrl}/api/v1/news/experience-stories`),

        axios.get(`${axiosBaseUrl}/api/v1/universities/university-profiles`),
        axios.get(`${axiosBaseUrl}/api/v1/universities/study-programmes`),

        axios.get(`${axiosBaseUrl}/api/v1/content/terms`),
        axios.get(`${axiosBaseUrl}/api/v1/content/faqs`),
        axios.get(`${axiosBaseUrl}/api/v1/content/admissions`),
      ]);


      toRet.push('news/everything');
      toRet.push('news/news');
      toRet.push('news/events');
      toRet.push('news/student-testimonials');
      toRet.push('news/experience-stories');


      for (let i = 0; i < news.data.data.length; i++) {
        const item = news.data.data[i];
        let slug = item['slug'];

        toRet.push(stringInject('article/{tab}/{slug}', {
            tab: 'news',
            slug: slug
          })
        );
      };
      for (let i = 0; i < events.data.data.length; i++) {
        const item = events.data.data[i];
        let slug = item['slug'];

        toRet.push(stringInject('article/{tab}/{slug}', {
            tab: 'events',
            slug: slug
          })
        );
      };
      for (let i = 0; i < studenttestimonials.data.data.length; i++) {
        const item = studenttestimonials.data.data[i];
        let slug = item['slug'];

        toRet.push(stringInject('article/{tab}/{slug}', {
            tab: 'student-testimonials',
            slug: slug
          })
        );
      };
      for (let i = 0; i < experiencestories.data.data.length; i++) {
        const item = experiencestories.data.data[i];
        let slug = item['slug'];

        toRet.push(stringInject('article/{tab}/{slug}', {
            tab: 'experience-stories',
            slug: slug
          })
        );
      };


      for (let i = 0; i < universities.data.data.length; i++) {
        const item = universities.data.data[i];
        let slug = item['slug'];

        toRet.push(stringInject('universities/{slug}', {
            slug: slug
          })
        );
      };
      for (let i = 0; i < studies.data.data.length; i++) {
        const item = studies.data.data[i];
        let slug = item['slug'];

        toRet.push(stringInject('studies/{slug}', {
            slug: slug
          })
        );
      };


      for (let i = 0; i < termsCategories.data.data.length; i++) {
        const item = termsCategories.data.data[i];
        let slug = item['slug'];

        toRet.push(stringInject('terms/{slug}', {
            slug: slug
          })
        );
      };
      for (let i = 0; i < faqsCategories.data.data.length; i++) {
        const item = faqsCategories.data.data[i];
        let slug = item['slug'];

        toRet.push(stringInject('faqs/{slug}', {
            slug: slug
          })
        );
      };
      for (let i = 0; i < admissionsCategories.data.data.length; i++) {
        const group = admissionsCategories.data.data[i];
        let groupslug = group['slug'];

        toRet.push(stringInject('admissions/{tab}', {
            tab: groupslug
          })
        );

        for (let i = 0; i < group.admissions.length; i++) {
          const item = group.admissions[i];
          let slug = item['slug'];

          toRet.push(stringInject('admissions/{tab}/{slug}/', {
              tab: groupslug,
              slug: slug
            })
          );
        }
      };

      return toRet;
    } */
  },

  redirect: [

    // { from: "^/news",                     to: "/news/news", statusCode: 301, },
    { from: "^/calendar",                 to: "/news/events", statusCode: 301, },
    { from: "^/testimonials",             to: "/students", statusCode: 301, },
    { from: "^/why-latvia",               to: "/latvia/why", statusCode: 301, },
    { from: "^/study-programmes",         to: "/studies", statusCode: 301, },
    // { from: "^/universities",             to: "/universities", statusCode: 301, },
    { from: "^/higher_education_latvia",  to: "/higher-education", statusCode: 301, },
    { from: "^/plan_your_studies",        to: "/admission/entrance-requirements", statusCode: 301, },
    { from: "^/entrance-requirements",    to: "/admission/entrance-requirements", statusCode: 301, },
    // { from: "^/admission",                to: "/admission/application-guide", statusCode: 301, },
    { from: "^/tuition_fees",             to: "/admission/fees-costs", statusCode: 301, },
    { from: "^/visa_residence_permits",   to: "/admission/visas-permits", statusCode: 301, },
    { from: "^/FAQ",                      to: "/faq/about-study-in-latvia", statusCode: 301, },
    // { from: "^/scholarships",             to: "/scholarships", statusCode: 301, },
    { from: "^/erasmus_plus",             to: "/higher-education", statusCode: 301, },
    { from: "^/about_latvia",             to: "/latvia/live", statusCode: 301, },
    // { from: "^/about",                    to: "/about", statusCode: 301, },
    { from: "^/accessibility-statement",  to: "/terms/accessibility-terms", statusCode: 301, },
  ],

  version: 1.1,
  cache: {
    // if you're serving multiple host names (with differing
    // results) from the same server, set this option to true.
    // (cache keys will be prefixed by your host name)
    // if your server is behind a reverse-proxy, please use
    // express or whatever else that uses 'X-Forwarded-Host'
    // header field to provide req.hostname (actual host name)
    useHostPrefix: false,
    pages: [
      // these are prefixes of pages that need to be cached
      // if you want to cache all pages, just include '/'
      '/',

      // you can also pass a regular expression to test a path
      // /^\/page3\/\d+$/,

      // to cache only root route, use a regular expression
      // /^\/$/
    ],

    // key(route, context) {
      // custom function to return cache key, when used previous
      // properties (useHostPrefix, pages) are ignored. return
      // falsy value to bypass the cache
    // },

    store: {
      // type: 'memory',
      type: process.env.STORE_TYPE || 'memory',

      // maximum number of pages to store in memory
      // if limit is reached, least recently used page
      // is removed.
      max: 50,

      // number of seconds to store this page in cache
      ttl: 60,

      options: {
        hosts: ['127.0.0.1:11211'],
      },
    },
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    transpile: [
      /vue-intersect/
    ]
  },

  server: {
    port: process.env.PORT || 3000, // default: 3000,
    host: process.env.HOST || "localhost", // default: localhost
  },

}
