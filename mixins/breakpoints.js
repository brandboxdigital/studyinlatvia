export default {
  computed: {
    isDesktop() {
      return this.$mq !== 'sm' && this.$mq !== 'md';
    }
  },
}
