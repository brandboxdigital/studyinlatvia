import {safeGet} from '~/plugins/helpers';

export default {

  methods: {
    _safeGet(obj, param, def = null) {
      return safeGet(obj, param, def);
    },
  },
}
